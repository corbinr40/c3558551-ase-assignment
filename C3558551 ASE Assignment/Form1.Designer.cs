﻿
namespace C3558551_ASE_Assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBoxCLI = new System.Windows.Forms.TextBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItemNew = new System.Windows.Forms.MenuItem();
            this.menuItemOpen = new System.Windows.Forms.MenuItem();
            this.menuItemSave = new System.Windows.Forms.MenuItem();
            this.menuItemQuit = new System.Windows.Forms.MenuItem();
            this.menuItemHelp = new System.Windows.Forms.MenuItem();
            this.buttonRun = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelWidthHeight = new System.Windows.Forms.Label();
            this.labelFillToggled = new System.Windows.Forms.Label();
            this.labelPenColour = new System.Windows.Forms.Label();
            this.richTextBoxConsole = new System.Windows.Forms.RichTextBox();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.richTextBox1.Location = new System.Drawing.Point(630, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(238, 538);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // textBoxCLI
            // 
            this.textBoxCLI.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxCLI.Location = new System.Drawing.Point(12, 439);
            this.textBoxCLI.Name = "textBoxCLI";
            this.textBoxCLI.Size = new System.Drawing.Size(530, 20);
            this.textBoxCLI.TabIndex = 2;
            this.textBoxCLI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCLI_ReturnKeyPressed);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem2,
            this.menuItemHelp});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemNew,
            this.menuItemOpen,
            this.menuItemSave,
            this.menuItemQuit});
            this.menuItem1.Text = "File";
            // 
            // menuItemNew
            // 
            this.menuItemNew.Index = 0;
            this.menuItemNew.Text = "New";
            this.menuItemNew.Click += new System.EventHandler(this.menuItemNew_Click);
            // 
            // menuItemOpen
            // 
            this.menuItemOpen.Index = 1;
            this.menuItemOpen.Text = "Open";
            this.menuItemOpen.Click += new System.EventHandler(this.menuItemOpen_Click);
            // 
            // menuItemSave
            // 
            this.menuItemSave.Index = 2;
            this.menuItemSave.Text = "Save";
            this.menuItemSave.Click += new System.EventHandler(this.menuItemSave_Click);
            // 
            // menuItemQuit
            // 
            this.menuItemQuit.Index = 3;
            this.menuItemQuit.Text = "Quit";
            this.menuItemQuit.Click += new System.EventHandler(this.menuItemQuit_Click);
            // 
            // menuItemHelp
            // 
            this.menuItemHelp.Index = 2;
            this.menuItemHelp.Text = "Help";
            this.menuItemHelp.Click += new System.EventHandler(this.menuItemHelp_Click);
            // 
            // buttonRun
            // 
            this.buttonRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRun.Location = new System.Drawing.Point(548, 439);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(75, 23);
            this.buttonRun.TabIndex = 4;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(611, 420);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // labelWidthHeight
            // 
            this.labelWidthHeight.AutoSize = true;
            this.labelWidthHeight.Location = new System.Drawing.Point(501, 0);
            this.labelWidthHeight.Name = "labelWidthHeight";
            this.labelWidthHeight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelWidthHeight.Size = new System.Drawing.Size(123, 13);
            this.labelWidthHeight.TabIndex = 6;
            this.labelWidthHeight.Text = "Width: XXX Height: XXX";
            this.labelWidthHeight.Click += new System.EventHandler(this.labelWidthHeight_Click);
            // 
            // labelFillToggled
            // 
            this.labelFillToggled.AutoSize = true;
            this.labelFillToggled.Location = new System.Drawing.Point(406, 0);
            this.labelFillToggled.Name = "labelFillToggled";
            this.labelFillToggled.Size = new System.Drawing.Size(89, 13);
            this.labelFillToggled.TabIndex = 7;
            this.labelFillToggled.Text = "Fill Toggled: True";
            // 
            // labelPenColour
            // 
            this.labelPenColour.AutoSize = true;
            this.labelPenColour.Location = new System.Drawing.Point(308, 0);
            this.labelPenColour.Name = "labelPenColour";
            this.labelPenColour.Size = new System.Drawing.Size(92, 13);
            this.labelPenColour.TabIndex = 8;
            this.labelPenColour.Text = "Pen Colour: Black";
            // 
            // richTextBoxConsole
            // 
            this.richTextBoxConsole.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBoxConsole.Location = new System.Drawing.Point(0, 468);
            this.richTextBoxConsole.Name = "richTextBoxConsole";
            this.richTextBoxConsole.Size = new System.Drawing.Size(630, 70);
            this.richTextBoxConsole.TabIndex = 9;
            this.richTextBoxConsole.Text = "";
            this.richTextBoxConsole.TextChanged += new System.EventHandler(this.richTextBoxConsole_TextChanged);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "Check Code";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 538);
            this.Controls.Add(this.richTextBoxConsole);
            this.Controls.Add(this.labelPenColour);
            this.Controls.Add(this.labelFillToggled);
            this.Controls.Add(this.labelWidthHeight);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.textBoxCLI);
            this.Controls.Add(this.richTextBox1);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBoxCLI;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItemNew;
        private System.Windows.Forms.MenuItem menuItemOpen;
        private System.Windows.Forms.MenuItem menuItemSave;
        private System.Windows.Forms.MenuItem menuItemQuit;
        private System.Windows.Forms.MenuItem menuItemHelp;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Label labelWidthHeight;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelFillToggled;
        private System.Windows.Forms.Label labelPenColour;
        private System.Windows.Forms.RichTextBox richTextBoxConsole;
        private System.Windows.Forms.MenuItem menuItem2;
    }
}

