﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;

namespace C3558551_ASE_Assignment
{
    /// <summary>
    /// Once called, a circle will be drawn on the canvas. It will be the same colour as current pen colour, and will be filled if fill has been toggled.
    /// </summary>
    class Circle
    {
        /// <summary>
        /// A circle will be drawn with a given integer, with the set pen colour and either filled or not filled dependent on the toggle.
        /// </summary>
        /// <param name="Radius (int)">A given integer for the radius for the circle</param>
        /// <param name="form1 (Form1)">An object of the main class</param>
        public void drawCircle(int Radius, Form1 form1)
        {

            Console console = new Console();
            int StartX = form1.CurrentX;
            int StartY = form1.CurrentY;

            form1.Radius = Radius;

            form1.paintGraphics.DrawEllipse(Pens.Black, StartX, StartY, Radius*2, Radius * 2);

            form1.PictureBox1.Refresh();

            if (form1.CurrentPenColour =="red/green")
            {
                Thread flashingCircleRedGreen = new Thread(() => Circle.redgreen(Radius, StartX, StartY, form1));
                flashingCircleRedGreen.Start();
            }
            else if(form1.CurrentPenColour == "blue/yellow")
            {
                Thread flashingCircleBlueYellow = new Thread(() => Circle.blueyellow(Radius, StartX, StartY, form1));
                flashingCircleBlueYellow.Start();
            }
            else if(form1.CurrentPenColour == "black/white")
            {
                Thread flashingCircleBlackWhile = new Thread(() => Circle.blackwhite(Radius, StartX, StartY, form1));
                flashingCircleBlackWhile.Start();
            }
            else {
                if (form1.FillToggled == true)
                {
                    if (form1.CurrentPenColour == "black")
                    {
                        form1.paintGraphics.FillEllipse(Brushes.Black, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "blue")
                    {
                        form1.paintGraphics.FillEllipse(Brushes.Blue, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "red")
                    {
                        form1.paintGraphics.FillEllipse(Brushes.Red, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "green")
                    {
                        form1.paintGraphics.FillEllipse(Brushes.Green, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }
                    else
                    {
                        console.consoleParser("Fill is toggled True, but can't determain pen colour", "exception", form1);
                    }
                }

                else if (form1.FillToggled == false)
                {
                    if (form1.CurrentPenColour == "black")
                    {
                        form1.paintGraphics.DrawEllipse(Pens.Black, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "blue")
                    {
                        form1.paintGraphics.DrawEllipse(Pens.Blue, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "red")
                    {
                        form1.paintGraphics.DrawEllipse(Pens.Red, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "green")
                    {
                        form1.paintGraphics.DrawEllipse(Pens.Green, StartX, StartY, Radius * 2, Radius * 2);

                        form1.PictureBox1.Refresh();
                    }
                    else
                    {
                        console.consoleParser("Fill is toggled False, but can't determain pen colour", "exception", form1);
                    }
                }
            }


        }
            
        public static void redgreen(int Radius,int StartX, int StartY, Form1 form1)
        {
            try
            {
                form1.paintGraphics.DrawEllipse(Pens.Red, StartX, StartY, Radius * 2, Radius * 2);

                if (form1.PictureBox1.InvokeRequired)
                {
                    form1.PictureBox1.Invoke(new MethodInvoker(
                        delegate ()
                        {
                            form1.PictureBox1.Refresh();
                        }));
                }
                else
                    form1.PictureBox1.Refresh();

                Thread.Sleep(500);
                form1.paintGraphics.DrawEllipse(Pens.Green, StartX, StartY, Radius * 2, Radius * 2);

                if (form1.PictureBox1.InvokeRequired)
                {
                    try
                    {
                        form1.PictureBox1.Invoke(new MethodInvoker(
                            delegate ()
                            {
                                form1.PictureBox1.Refresh();
                            }));
                    }
                    catch (ThreadStateException)
                    {
                        System.Console.WriteLine("AHHH");
                    }
                }
                else
                    form1.PictureBox1.Refresh();

                Thread.Sleep(500);
                redgreen(Radius, StartX, StartY, form1);
            }
            catch (ThreadStateException)
            {
                System.Console.WriteLine("Ahh");
            }
        }
        public static void blueyellow(int Radius, int StartX, int StartY, Form1 form1)
        {
            form1.paintGraphics.DrawEllipse(Pens.Blue, StartX, StartY, Radius * 2, Radius * 2);
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            

            form1.paintGraphics.DrawEllipse(Pens.Yellow, StartX, StartY, Radius * 2, Radius * 2);
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            blueyellow(Radius, StartX, StartY, form1);
        }
        public static void blackwhite(int Radius, int StartX, int StartY, Form1 form1)
        {

            form1.paintGraphics.DrawEllipse(Pens.Black, StartX, StartY, Radius * 2, Radius * 2);
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);

            form1.paintGraphics.DrawEllipse(Pens.White, StartX, StartY, Radius * 2, Radius * 2);
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            blackwhite(Radius, StartX, StartY, form1);
        }
    }
}
