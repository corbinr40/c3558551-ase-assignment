﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace C3558551_ASE_Assignment
{
    /// <summary>
    /// 
    /// 
    /// </summary>
    public partial class Form1: Form
    {
        /// <summary>
        /// The program starts
        /// </summary>
        /// <param name="testBoxCode"></param>
        /// <param name="testCLICode"></param>
        /// <param name="paintImage"></param>
        /// <param name="paintGraphics"></param>
        /// <param name="fillToggled"></param>
        /// <param name="currentPenColour"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="radius"></param>
        /// <param name="lengthAB"></param>
        /// <param name="lengthAC"></param>
        /// <param name="angleA"></param>

        String textBoxCode = "";
        String textCLICode = "";

        public Bitmap paintImage;
        public Graphics paintGraphics;

        private bool fillToggled;
        private string currentPenColour = "black";

        private int height;
        private int width;
        private int radius;

        private int lengthAB;
        private int lengthAC;
        private int angleA;

        private int currentX;
        private int currentY = 0;

        private string commandOutputType;
        private string commandOutputs;

        private int currentLine;


        /**
         * Getters and Setters for the currentX and currentY locations
         * 
         * */

        /// <summary>
        /// A Getter and Setter of the current value of the X location.
        ///</summary>
        ///<param name= "currentX (int)">The current location of the X Value for the cursor location.</param>
        ///<returns>The value of the X Location</returns>
        public int CurrentX
        {
            get { return this.currentX; }
            set { this.currentX = value; }
        }

        /// <summary>
        /// A Getter and Setter of the current value of the Y location.
        ///</summary>
        ///<param name= "currentY (int)">The current location of the Y Value for the cursor location.</param>
        ///<returns>The value of the Y Location</returns>
        public int CurrentY
        {
            get { return this.currentY; }
            set { this.currentY = value; }
        }

        /// <summary>
        /// A Getter and Setter of the PictureBox object, which houses the drawing canvas.
        ///</summary>
        ///<param name= "pictureBox1 (PictureBox)">The current state of the graphics canvas.</param>
        ///<returns>The graphics canvas</returns>
        public PictureBox PictureBox1
        {
            get { return this.pictureBox1; }
            set { this.pictureBox1 = value; }
        }

        /// <summary>
        /// A Getter and Setter to see if the Fill command has been called, or toggled.
        ///</summary>
        ///<param name= "FillToggled (bool)">The current state of Fill, default being false.</param>
        ///<returns>The value of FillToggled being either True or False</returns>
        public bool FillToggled
        {
            get { return this.fillToggled; }
            set { this.fillToggled = value; }
        }

        /// <summary>
        /// A Getter and Setter of the current value of the Pen Colour state.
        ///</summary>
        ///<param name= "currentPenColour (string)">The current state of the Pen Colour.</param>
        ///<returns>The value of the Pen Colour</returns>
        public string CurrentPenColour
        {
            get { return this.currentPenColour; }
            set { this.currentPenColour = value; }
        }

        /// <summary>
        /// A Getter and Setter of the Height for the rectangle.
        ///</summary>
        ///<param name= "Height (int)">The Height of the rectangle.</param>
        ///<returns>The value of a rectangle Height</returns>
        public int Height
        {
            get { return this.height; }
            set { this.height = value; }
        }

        /// <summary>
        /// A Getter and Setter of the Width for the rectangle.
        ///</summary>
        ///<param name= "Width (int)">The Width of the rectangle.</param>
        ///<returns>The value of a rectangle Width</returns>
        public int Width
        {
            get { return this.width; }
            set { this.width = value; }
        }

        /// <summary>
        /// A Getter and Setter of the Radius for the circle.
        ///</summary>
        ///<param name= "Radius (int)">The Radius of the circle.</param>
        ///<returns>The value of a circle Radius</returns>
        public int Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        /// <summary>
        /// A Getter and Setter of the side LengthAB for the triangle.
        ///</summary>
        ///<param name= "LengthAB (int)">The side LengthAB of the triangle.</param>
        ///<returns>The value of a triangle side LengthAB</returns>
        public int LengthAB
        {
            get { return this.lengthAB; }
            set { this.lengthAB = value; }
        }

        /// <summary>
        /// A Getter and Setter of the side LengthAC for the triangle.
        ///</summary>
        ///<param name= "LengthAC (int)">The side LengthAC of the triangle.</param>
        ///<returns>The value of a triangle side LengthAC</returns>
        public int LengthAC
        {
            get { return this.lengthAC; }
            set { this.lengthAC = value; }
        }

        /// <summary>
        /// A Getter and Setter of the angle AngleA for the triangle.
        ///</summary>
        ///<param name= "AngleA (int)">The angle AngleA of the triangle.</param>
        ///<returns>The value of a triangle angle AngleA</returns>
        public int AngleA
        {
            get { return this.angleA; }
            set { this.angleA = value; }
        }

        public Label LabelFillToggled
        {
            get { return this.labelFillToggled; }
            set { this.labelFillToggled = value; }
        }

        public Label LabelPenColour
        {
            get { return this.labelPenColour; }
            set { this.labelPenColour = value; }
        }

        public string CommandOutputs
        {
            get { return this.commandOutputs; }
            set { this.commandOutputs = value; }
        }
        
        public string CommandOutputType
        {
            get { return this.commandOutputType; }
            set { this.commandOutputType = value; }
        }

        public RichTextBox RichTextBoxConsole
        {
            get { return this.richTextBoxConsole; }
            set { this.richTextBoxConsole = value; }
        }

        /// <summary>
        /// Getter and Setters for a customVariable Dictionary
        /// </summary>
        public Dictionary<string, int> customVariableDict = new Dictionary<string, int>();

        /// <summary>
        /// Getter and Setters for a customMethod Dictionary
        /// </summary>
        public Dictionary<string, string> customMethodDict = new Dictionary<string, string>();

        /// <summary>
        /// Getter and Setters for a customMethodVari Dictionary
        /// </summary>
        public Dictionary<string, string[]> customMethodVari = new Dictionary<string, string[]>();

        public int CurrentLineNo
        {
            get { return this.currentLine; }
            set { this.currentLine = value; }
        }

        Commands commands = new Commands();

        Console console = new Console();


        /// <summary>
        /// Initialising the Form1 class and setting a lablel to show the Width and Height of the drawing canvas.
        ///</summary>
        public Form1()
        {
            InitializeComponent();

            labelWidthHeight.Text = $"Width: {pictureBox1.Width} Height: {pictureBox1.Height}";

            LabelFillToggled.Text = $"Fill Toggled: {FillToggled.ToString()}";
            LabelPenColour.Text = $"Pen Colour: {CurrentPenColour.ToString()}";
        }

        /// <summary>
        /// On the Form loading, it will call the method newCanvas.<br></br>
        /// newCanvas will load the drawing canvas.<br></br>
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is called.</param>
        private void Form1_Load(object sender, EventArgs e)
        {

            //CurrentPenColour = Pens.Black;

            newCanvas();

            menuItemNew.Shortcut = Shortcut.CtrlN;
            menuItemOpen.Shortcut = Shortcut.CtrlO;
            menuItemSave.Shortcut = Shortcut.CtrlS;
            menuItemQuit.Shortcut = Shortcut.CtrlQ;
            menuItemHelp.Shortcut = Shortcut.CtrlH;

            RichTextBoxConsole.BackColor = Color.Black;
            RichTextBoxConsole.ReadOnly = true;
            RichTextBoxConsole.ScrollBars = RichTextBoxScrollBars.Vertical;

        }

        /// <summary>
        /// On the Run button clicked, it will get the large text box code, set all the text to lower case, and remove all spaces.<br></br>
        /// Then it will assign it to the string variable "textBoxCode".<br></br>
        /// Next it will pass that variable to the Commands Class, and the commandPasser method.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            textBoxCode = richTextBox1.Text.ToString().ToLower().Replace(" ", "");

            commands.commandParser(textBoxCode, true, true, this);
        }

        /// <summary>
        /// This will call a new canvas from the canvas class, whilst passing the current class and an object.
        ///</summary>
        public void newCanvas()
        {
            Canvas canvas = new Canvas();

            canvas.loadPictureBox(this);
        }

        /// <summary>
        /// On the Width and Height label being clicked, it will output a message to the IDE's console.<br></br>
        /// This is something that will be implmented later on in development.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void labelWidthHeight_Click(object sender, EventArgs e)
        {
            console.consoleParser("Label Clicked", "system", this);
            console.consoleParser("On Click, user can enter the Width and height of the graphics box.\nProgram will restart according to new numbers.", "system", this);
        }

        /// <summary>
        /// On the menu item "new" being clicked, it will clear all text boxes, canvas' and refresh the canvas.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void menuItemNew_Click(object sender, EventArgs e)
        {
            console.consoleParser("New Button Clicked", "system", this);
            Canvas canvas = new Canvas();
            
            richTextBox1.Clear();
            richTextBox1.Focus();

            canvas.loadPictureBox(this);
        }

        /// <summary>
        /// On the menu item "open" being clicked, it will open a dialoge box for a text file to be selected.<br></br>
        /// Once a file has been selected, the text file contents will be moved into the textbox.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void menuItemOpen_Click(object sender, EventArgs e)
        {
            console.consoleParser("Open Button Clicked", "system", this);

            using (OpenFileDialog openFile = new OpenFileDialog())
            {

                //openFile.Filter = "Text files(*.txt)| *.txt | All files(*.*) | *.* ";
                //openFile.FilterIndex = 2;

                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    richTextBox1.Text = File.ReadAllText(openFile.FileName);
                }
            }
        }

        /// <summary>
        /// On the menu item "save" being clicked, it will open a dialoge box for a save location.<br></br>
        /// Once a location has been selected, it will save the file at the location and as plain text.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void menuItemSave_Click(object sender, EventArgs e)
        {
            console.consoleParser("Save Button Clicked", "system", this);

            using (SaveFileDialog saveFile = new SaveFileDialog())
            {

                //openFile.Filter = "Text files(*.txt)| *.txt | All files(*.*) | *.* ";
                //openFile.FilterIndex = 2;

                saveFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    //richTextBox1.SaveFile(saveFile.InitialDirectory, RichTextBoxStreamType.RichText);
                    richTextBox1.SaveFile(saveFile.FileName, RichTextBoxStreamType.PlainText);
                }
            }
        }

        /// <summary>
        /// On the menu item "quit" being clicked, it will close the current instance of the program.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void menuItemQuit_Click(object sender, EventArgs e)
        {
            console.consoleParser("Quit Button Clicked", "system", this);
            this.Close();
        }

        /// <summary>
        /// On the menu item "help" being clicked, it will open a message box saying that the help.<br></br>
        /// This is currently temporary. In the future, I would want this to give the command options for the user.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void menuItemHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Help Button Clicked");
        }

        /// <summary>
        /// When the return key is pressed onn the Single Command Line Interface, it will remove all the spaces and set the text to entirely lowercase and then, send the command to the Commands Class to the commandPasser Method.<br></br>
        /// If the word "run" is typed in the CLI, it will get the code from the large code text box and run through that in a similar manor as previously said.
        ///</summary>
        ///<param name= "sender (object)">A reference to control the object raised by an event.</param>
        ///<param name= "e (EventArgs)">The event data sent from when the object is clicked.</param>
        private void textBoxCLI_ReturnKeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {

                if (textBoxCLI.Text.Contains("run"))
                {

                    textBoxCode = richTextBox1.Text.ToString().ToLower().Replace(" ", "");

                    try
                    {
                        commands.commandParser(textBoxCode, true, true, this);
                    }
                    catch
                    {
                        console.consoleParser("No commands entered", "system", this);
                    }
                }
                else 
                {
                    textCLICode = textBoxCLI.Text.ToString().ToLower().Replace(" ", "");

                    try
                    {
                        textBoxCLI.Text = "";
                        commands.commandParser(textCLICode, true, true, this);
                    }
                    catch
                    {
                        console.consoleParser("No commands entered", "exception", this);
                    }
                }


            }
        }

        private void richTextBoxConsole_TextChanged(object sender, EventArgs e)
        {
            //richTextBoxConsole
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            textBoxCode = richTextBox1.Text.ToString().ToLower().Replace(" ", "");

            commands.commandParser(textBoxCode, false, true, this);
        }
    }
}
