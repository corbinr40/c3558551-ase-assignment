﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C3558551_ASE_Assignment
{
    public class CommandsAdvanced
    {
        Console console = new Console();

        public void variableSum(String variableName, int variableValue, Form1 form1)
        {
            form1.customVariableDict[variableName] = ((form1.customVariableDict[variableName]) + variableValue);
        }

        public int ConditionalParser(String[] commandList, String conditionType, bool runButton, Form1 form1)
        {
            String[] loopArray = default;
            Char symbol = '&';
            int variableDictValue = 0;
            int variableValue = 0;
            String[] loop;
            int count = 0;
            int arrayCount = 0;
            int startCount = 0;
            int finCount = 0;
            bool whileTag = false;

            if (conditionType == "while")
            {
                if ((commandList[(form1.CurrentLineNo - 1)].ToString()).Contains('<'))
                {
                    loopArray = commandList[(form1.CurrentLineNo - 1)].ToString().Replace(" ", "").Remove(0, 5).Split('<');
                    
                    try
                    {
                        variableDictValue = form1.customVariableDict[loopArray[0]];
                        variableValue = Int32.Parse(loopArray[1]);

                        symbol = '<';

                    }
                    catch (Exception e)
                    {

                    }

                }
                else if ((commandList[(form1.CurrentLineNo - 1)].ToString()).Contains('>'))
                {
                    loopArray = commandList[(form1.CurrentLineNo - 1)].ToString().Replace(" ", "").Remove(0, 5).Split('>');

                    try
                    {
                        variableDictValue = form1.customVariableDict[loopArray[0]];
                        variableValue = Int32.Parse(loopArray[1]);

                        symbol = '>';
                    }
                    catch (Exception e)
                    {

                    }

                }
                else if ((commandList[(form1.CurrentLineNo - 1)].ToString()).Contains('='))
                {
                    loopArray = commandList[(form1.CurrentLineNo - 1)].ToString().Replace(" ", "").Remove(0, 5).Split('=');

                    try
                    {
                        variableDictValue = form1.customVariableDict[loopArray[0]];
                        variableValue = Int32.Parse(loopArray[1]);

                        symbol = '=';

                    }
                    catch (Exception e)
                    {
                        
                    }

                }
                else
                {
                    console.consoleParser("Ahhh, its failed", "exception", form1);
                }


                foreach (var commands in commandList)
                {
                    if (commands.Contains("endwhile"))
                    {
                        whileTag = false;
                        finCount = count;
                    }
                    else if (commands.Contains("while"))
                    {
                        whileTag = true;
                        startCount = count;
                    }
                    else
                    {

                    }

                    if (whileTag == true)
                    {
                        arrayCount++;
                    }
                    else
                    {

                    }

                    count++;
                }

                loop = new String[arrayCount];

                int i = 0;
                foreach (int value in Enumerable.Range(startCount, finCount))
                {
                    try
                    {
                        loop[i] = commandList[value];
                        i++;
                    }
                    catch (IndexOutOfRangeException e)
                    {

                    }
                }

                loop = loop.Where((source, index) => index != 0).ToArray();
                try
                {
                    whileParserSuccess(symbol, loop, form1.customVariableDict[loopArray[0]], variableValue, loopArray, runButton, form1);
                }
                catch (KeyNotFoundException)
                {
                    Console console = new Console();
                    console.consoleParser("Incorrect varable added at like: " + form1.CurrentLineNo,"exception",form1);
                }
            }
            else if (conditionType == "if")
            {
                if ((commandList[(form1.CurrentLineNo - 1)].ToString()).Contains('<'))
                {
                    loopArray = commandList[(form1.CurrentLineNo - 1)].ToString().Replace(" ", "").Remove(0, 2).Split('<');

                    variableDictValue = form1.customVariableDict[loopArray[0]];
                    variableValue = Int32.Parse(loopArray[1]);

                    symbol = '<';
                    try
                    {
                        System.Console.WriteLine("Custom Variable Dictionary value: " + form1.customVariableDict[loopArray[0]]);
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine("No Variable Found in dictionary: " + loopArray[0] + "\nError Code: " + e);
                    }

                }
                else if ((commandList[(form1.CurrentLineNo - 1)].ToString()).Contains('>'))
                {
                    loopArray = commandList[(form1.CurrentLineNo - 1)].ToString().Replace(" ", "").Remove(0, 2).Split('>');
                    variableDictValue = form1.customVariableDict[loopArray[0]];
                    variableValue = Int32.Parse(loopArray[1]);
                    symbol = '>';
                    try
                    {
                        System.Console.WriteLine("Custom Variable Dictionary value: " + form1.customVariableDict[loopArray[0]]);
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine("No Variable Found in dictionary: " + loopArray[0] + "\nError Code: " + e);
                    }

                }
                else if ((commandList[(form1.CurrentLineNo - 1)].ToString()).Contains('='))
                {
                    loopArray = commandList[(form1.CurrentLineNo - 1)].ToString().Replace(" ", "").Remove(0, 2).Split('=');
                    variableDictValue = form1.customVariableDict[loopArray[0]];
                    variableValue = Int32.Parse(loopArray[1]);
                    symbol = '=';
                    try
                    {
                        System.Console.WriteLine("Custom Variable Dictionary value: " + form1.customVariableDict[loopArray[0]]);
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine("No Variable Found in dictionary: " + loopArray[0] + "\nError Code: " + e);
                    }

                }
                else
                {
                    console.consoleParser("Ahhh, its failed", "exception", form1);
                }

                foreach (var commands in commandList)
                {
                    if (commands.Contains("endif"))
                    {
                        whileTag = false;
                        finCount = count;
                    }
                    else if (commands.Contains("if"))
                    {
                        whileTag = true;
                        startCount = count;
                    }
                    else
                    {

                    }

                    if (whileTag == true)
                    {
                        arrayCount++;
                    }
                    else
                    {

                    }

                    count++;
                }
                loop = new String[(arrayCount)];

                int i = 0;
                foreach (int value in Enumerable.Range((startCount + 1), (finCount-1)))
                {
                    try
                    {
                        loop[i] = commandList[value];
                        i++;
                    }
                    catch (IndexOutOfRangeException e)
                    {

                    }
                }

                loop = loop.Where((source, index) => index != 0).ToArray();

                ifParserSuccess(symbol, loop, variableValue, loopArray, runButton, form1);
            }
            return form1.CurrentLineNo;
        }

        private void ifParserSuccess(Char symbol, String[] loop, int variableValue, String[] loopArray, bool runButton, Form1 form1)
        {
            if (symbol == '<')
            {
                if (form1.customVariableDict[loopArray[0]] < variableValue)
                {

                    Commands commands = new Commands();
                    foreach (var command in loop)
                    {
                        form1.CurrentLineNo++;
                        commands.commandParser(command, true, false, form1);
                    }
                }
                else
                {
                    foreach (var commands2 in loop)
                    {
                        form1.CurrentLineNo++;
                    }
                }
            }
            else if (symbol == '>')
            {
                if (form1.customVariableDict[loopArray[0]] > variableValue)
                {

                    Commands commands = new Commands();
                    foreach (var commands2 in loop)
                    {
                        commands.commandParser(commands2, runButton, false, form1);
                    }
                }
                else 
                {
                    foreach (var commands2 in loop)
                    {
                        form1.CurrentLineNo++;
                    }
                }

            }
            else if (symbol == '=')
            {
                if (form1.customVariableDict[loopArray[0]] == variableValue)
                {
                    Commands commands = new Commands();
                    foreach (var commands2 in loop)
                    {
                        commands.commandParser(commands2, runButton, false, form1);
                    }
                }
                else
                {
                    foreach (var commands2 in loop)
                    {
                        form1.CurrentLineNo++;
                    }
                }

            }
            else
            {
                Console console = new Console();
                console.consoleParser("Invalid Expressing used: " + symbol, "exception", form1);
            }
        }

        private void whileParserSuccess(Char symbol, String[] loop, int temp, int variableValue, String[] loopArray, bool runButton, Form1 form1)
        {
            if (symbol == '<')
            {
                try
                {
                    while (form1.customVariableDict[loopArray[0]] < variableValue)
                    {
                        Commands commands = new Commands();
                        foreach (var commands2 in loop)
                        {
                            form1.CurrentLineNo++;
                            commands.commandParser(commands2, runButton, false, form1);
                        }
                    }
                }
                catch (Exception e)
                {
                    foreach (var commands2 in loop)
                    {
                        form1.CurrentLineNo++;
                    }
                }
            }
            else if (symbol == '>')
            {
                try
                {
                    while (form1.customVariableDict[loopArray[0]] > variableValue)
                    {
                        Commands commands = new Commands();
                        foreach (var commands2 in loopArray)
                        {
                            commands.commandParser(commands2, runButton, false, form1);
                        }
                    }
                }
                catch(Exception e)
                {
                    foreach (var commands2 in loop)
                    {
                        form1.CurrentLineNo++;
                    }
                }

            }
            else if (symbol == '=')
            {
                try
                {
                    while (form1.customVariableDict[loopArray[0]] == variableValue)
                    {
                        Commands commands = new Commands();
                        foreach (var commands2 in loopArray)
                        {
                            commands.commandParser(commands2, runButton, false, form1);
                        }
                    }
                }
                catch(Exception e)
                {
                    foreach (var commands2 in loop)
                    {
                        form1.CurrentLineNo++;
                    }
                }

            }
            else
            {
                Console console = new Console();
                console.consoleParser("Invalid Expressing used: " + symbol, "exception", form1);

            }
        }

        public int methodParser(String[] commandList, bool runButton, Form1 form1)
        {
            String[] methodSplitArray = default;
            String[] methodVariablesArray = default;
            int count = 0;
            int arrayCount = 0;
            int startCount = 0;
            int finCount = 0;
            bool methodTag = false;

            methodSplitArray = commandList[(form1.CurrentLineNo - 1)].Replace(" ", "").Remove(0, 6).Replace(")", "").Split('(');
            methodVariablesArray = methodSplitArray[1].Split(',');

            foreach (var commands in commandList)
            {
                if (commands.Contains("endmethod"))
                {
                    methodTag = false;
                    finCount = count;
                }
                else if (commands.Contains("method"))
                {
                    methodTag = true;
                    startCount = count;
                }
                else
                {
                    
                }

                if (methodTag == true)
                {
                    arrayCount++;
                }
                else
                {
                    
                }

                count++;
            }

            int i = 0;
            foreach (int value in Enumerable.Range((startCount + 1), (finCount - 1)))
            {
                try
                {
                    String keyName = (methodSplitArray[0].ToString() + i);

                    form1.customMethodDict[keyName] = commandList[value];

                    form1.CurrentLineNo++;
                    i++;
                }
                catch (IndexOutOfRangeException e)
                {

                }
            }

            form1.customMethodVari[methodSplitArray[0]] = methodVariablesArray;

            return form1.CurrentLineNo;
        }

        public void foundMethodParser(String methodName, String methodVariables, bool runButton, Form1 form1)
        {
            String[] foundMethodVarables;
            String[] savedMethodVarables;

            foundMethodVarables = methodVariables.Split(',');
            savedMethodVarables = form1.customMethodVari[methodName];

            for (int i = 0; i <= (foundMethodVarables.Length-1); i++)
            {
                form1.customVariableDict[savedMethodVarables[i]] = Int32.Parse(foundMethodVarables[i]);
            }

            bool methodLinesFinished = false;
            for (int i = 0; methodLinesFinished == false; i++)
            {
                String keyName = methodName + i;
                try
                {
                    Commands commands = new Commands();

                    commands.commandParser(form1.customMethodDict[keyName], runButton, false, form1);
                }
                catch (KeyNotFoundException e)
                {
                    methodLinesFinished = true;
                }
            }

        }
        
    }
}
