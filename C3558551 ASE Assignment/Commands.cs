﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace C3558551_ASE_Assignment
{
    /// <summary>
    /// This is where all the commands will be parsed through.
    /// </summary>
    public class Commands
    {
        /// <summary>
        /// The entire textbox's string will be passed through.<br></br>
        /// It will split the lines at the carrage return and add it to an array.<br></br>
        /// It will loop through the array until it has reached the end.<br></br>
        /// It will print to the console if the command has been found and any additonal information: <br></br> 
        /// i.e. Rectangle - Command Found <br></br> Width:30, Height: 10.
        /// </summary>
        /// <param name="textBoxCode (String)">The textbox's entry as a string</param>
        /// <param name="form1 (Form1)">An object of the main class</param>
        public void commandParser(String textBoxCode, bool runButton, bool freshRun, Form1 form1)
        {

            String[] commandList = textBoxCode.Split('\n');

            int tempCurrentLine = default;

            if (freshRun != true)
            {
                tempCurrentLine = form1.CurrentLineNo;
                form1.CurrentLineNo = 0;
            }

            Console console = new Console();
            if (runButton == true)
            {
                try
                {
                    var command = commandList[form1.CurrentLineNo];

                    form1.CurrentLineNo++;


                    /** Implemented
                        * Command requires user to input 2 points (X and Y) to move the current "cursor" location to a new one.
                        * This will be contained within this class.
                        **/

                    

                    if (command.Contains("moveto"))
                    {
                        try
                        {
                            console.consoleParser("MoveTo - Command found", "valid", form1);

                            String[] location = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                            try
                            {
                                console.consoleParser($"\nOld CurrentX: {form1.CurrentX} Old CurrentY: {form1.CurrentY}", "valid", form1);
                                int startX = Int32.Parse(location[0]);
                                int startY = Int32.Parse(location[1]);
                                console.consoleParser($"Start X: {startX}, Start Y: {startY}", "valid", form1);

                                form1.CurrentX = startX;
                                form1.CurrentY = startY;

                                console.consoleParser($"\ninputX: {startX} inputY: {startY}\nCurrentX: {form1.CurrentX} CurrentY: {form1.CurrentY}", "valid", form1);

                            }
                            catch (FormatException e)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command}", "exception", form1);
                            }
                            catch (ArgumentOutOfRangeException e)
                            {
                                console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    /** Implemented
                        * Command requires user to input 2 points (X and Y), this will draw a line from the current location to the new location.
                        * The user inputted location will become the "new current" location.
                        * This will call the CanvasDraw Class.
                        **/
                    else if (command.Contains("drawto"))
                    {
                        try
                        {
                            console.consoleParser("DrawTo - Command found", "valid", form1);

                            String[] location = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                            try
                            {
                                int oldX = form1.CurrentX;
                                int oldY = form1.CurrentY;

                                int newX = Int32.Parse(location[0]);
                                int newY = Int32.Parse(location[1]);
                                console.consoleParser($"Old X: {oldX}, Old Y: {oldY}\nNew X: {newX}, New Y: {newY}", "valid", form1);

                                CanvasDraw canvasDraw = new CanvasDraw();

                                form1.CurrentX = newX;
                                form1.CurrentY = newY;

                                canvasDraw.drawLine(oldX, oldY, newX, newY, form1);
                            }
                            catch (FormatException e)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Move To Out of Range Exception at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    /** IMPLEMENTED
                        * When this command is entered, it will clear the canvas.
                        * This will call the Canvas class.
                        **/
                    else if (command == "clear")
                    {

                        console.consoleParser("Clear - Command found", "valid", form1);

                        form1.newCanvas();
                    }

                    /** IMPLEMENTED
                        * 
                        * Reset the cursor location to (0,0)
                        **/
                    else if (command == "reset")
                    {
                        console.consoleParser("Reset - Command found", "valid", form1);

                        form1.CurrentX = 0;
                        form1.CurrentY = 0;
                    }

                    /** IMPLEMENTED
                        * The user will enter a "Height" and "Width" of the rectangle. 
                        * Once detected, everything will be removed apart from the numbers.
                        * Those numbers will be parsed to the Rectangle Class.
                        * The program will "draw" the rectangle starting from the current cursor position.
                        * Will call the Rectangle Class
                        **/
                    else if (command.Contains("rectangle"))
                    {
                        try
                        {
                            console.consoleParser("Rectangle - Command found", "valid", form1);

                            String[] rectangle = command.Remove(0, 9).Replace("(", "").Replace(")", "").Split(',');

                            Rectangle rectangle1 = new Rectangle();

                            try
                            {
                                int width = Int32.Parse(rectangle[0]);
                                int height = Int32.Parse(rectangle[1]);
                                console.consoleParser($"Line 193 Width: {width}, Height: {height}", "valid", form1);

                                rectangle1.drawRectangle(width, height, true, form1);
                            }
                            catch (FormatException e)
                            {
                                try
                                {
                                    console.consoleParser("Attempting Custom Dict value", "System", form1);
                                    if (form1.customVariableDict.ContainsKey(rectangle[0]))
                                    {
                                        int width = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                        int height;

                                        console.consoleParser($"Line 211 Width: {width}", "valid", form1);

                                        if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                        {
                                            height = Convert.ToInt32(form1.customVariableDict[rectangle[1]]);


                                            console.consoleParser($"Line 221 Height: {height}", "valid", form1);


                                        }
                                        else
                                        {
                                            height = Int32.Parse(rectangle[1]);
                                        }

                                        rectangle1.drawRectangle(width, height, true, form1);
                                    }
                                    else if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                    {

                                        int width = Int32.Parse(rectangle[0]);
                                        int height = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);

                                        console.consoleParser($"Line 238 Width: {width}", "valid", form1);

                                        rectangle1.drawRectangle(width, height, true, form1);

                                    }

                                }
                                catch (FormatException except)
                                {
                                    console.consoleParser($"Line 247. Execption Thrown: Format Exception. Expected Numbers from: {command}", "exception", form1);

                                }
                            }
                            catch
                            {
                                console.consoleParser($"Line 256 Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                        catch (FormatException e)
                        {
                            try
                            {
                                console.consoleParser("Rectangle - Command found", "valid", form1);

                                String[] rectangle = command.Remove(0, 9).Replace("(", "").Replace(")", "").Split(',');

                                Rectangle rectangle1 = new Rectangle();

                                console.consoleParser("Attempting Custom Dict value", "System", form1);
                                if (form1.customVariableDict.ContainsKey(rectangle[0]))
                                {
                                    int width = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                    int height;

                                    console.consoleParser($"Width: {width}", "valid", form1);

                                    if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                    {
                                        height = Convert.ToInt32(form1.customVariableDict[rectangle[1]]);


                                        console.consoleParser($"Height: {height}", "valid", form1);


                                    }

                                    height = Int32.Parse(rectangle[1]);
                                    rectangle1.drawRectangle(width, height, true, form1);
                                }
                                else if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                {
                                    int width = Int32.Parse(rectangle[0]);
                                    int height = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);

                                    console.consoleParser($"Width: {width}", "valid", form1);

                                    rectangle1.drawRectangle(width, height, true, form1);

                                }

                            }
                            catch (FormatException except)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "exception", form1);

                            }
                        }
                        catch
                        {
                            try
                            {
                                console.consoleParser("Rectangle - Command found", "valid", form1);

                                String[] rectangle = command.Remove(0, 9).Replace("(", "").Replace(")", "").Split(',');

                                Rectangle rectangle1 = new Rectangle();
                                if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                {
                                    int width = Int32.Parse(rectangle[0]);
                                    int height = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);

                                    console.consoleParser($"Width: {width}", "valid", form1);

                                    rectangle1.drawRectangle(width, height, true, form1);

                                }
                            }
                            catch
                            {
                                console.consoleParser($"Line 331 Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);

                            }

                        }


                    }

                    /** IMPLEMENTED
                        * A circle will be drawn as an Ellipse.
                        * The circle will require just the radius. 
                        * It will create a rectangle bounding box.
                        * Will call the Circle Class
                        **/
                    else if (command.Contains("circle"))
                    {
                        try
                        {
                            console.consoleParser("Circle - Command found", "valid", form1);

                            String[] circle = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                            Circle circle1 = new Circle();

                            try
                            {
                                int radius = Int32.Parse(circle[0]);
                                console.consoleParser($"Radius: {radius}", "valid", form1);

                                circle1.drawCircle(radius, form1);
                            }
                            catch (FormatException e)
                            {
                                try
                                {
                                    console.consoleParser("Attempting Custom Dict value", "System", form1);
                                    if (form1.customVariableDict.ContainsKey(circle[0]))
                                    {
                                        int radius = Convert.ToInt32(form1.customVariableDict[circle[0]]);

                                        console.consoleParser($"Radius: {radius}", "valid", form1);

                                        circle1.drawCircle(radius, form1);
                                    }

                                }
                                catch (FormatException except)
                                {
                                    console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command}", "exception", form1);
                                }
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            try
                            {
                                String[] circle = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                                Circle circle1 = new Circle();

                                console.consoleParser("Attempting Custom Dict value", "System", form1);
                                if (form1.customVariableDict.ContainsKey(circle[0]))
                                {
                                    int radius = Convert.ToInt32(form1.customVariableDict[circle[0]]);

                                    console.consoleParser($"Radius: {radius}", "valid", form1);

                                    circle1.drawCircle(radius, form1);
                                }

                            }
                            catch (FormatException except)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    /** IMPLEMENTED
                        * 
                        * 
                        **/
                    else if (command.Contains("triangle"))
                    {
                        try
                        {
                            console.consoleParser("Triangle - Command found", "valid", form1);

                            String[] triangle = command.Remove(0, 8).Replace("(", "").Replace(")", "").Split(',');

                            Triangle triangle1 = new Triangle();

                            try
                            {
                                int lengthAB = Int32.Parse(triangle[0]);
                                int lengthAC = Int32.Parse(triangle[1]);
                                int angleA = Int32.Parse(triangle[2]);

                                console.consoleParser($"LengthAB: {lengthAB}, LengthAC: {lengthAC}, AngleA: {angleA}", "valid", form1);

                                triangle1.drawTriangle(lengthAB, lengthAC, angleA, form1);
                            }
                            catch (FormatException e)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    else if (command.Contains("pen"))
                    {
                        /** IMPLEMENTED
                            * Each shape class will check what the "Current Pen Colour" is, then check draw dependent on the selected colour.
                            * 
                            * */
                        if (command == "penblack")
                        {
                            console.consoleParser("Pen Colour Black- Command found", "valid", form1);

                            form1.CurrentPenColour = "black";

                            form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";

                        }

                        /** IMPLEMENTED
                            * 
                            **/
                        else if (command == "penblue")
                        {
                            console.consoleParser("Pen Colour Blue - Command found", "valid", form1);
                            form1.CurrentPenColour = "blue";

                            form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";

                        }

                        /** IMPLEMENTED
                            * 
                            **/
                        else if (command == "penred")
                        {
                            console.consoleParser("Pen Colour Red - Command found", "valid", form1);
                            form1.CurrentPenColour = "red";

                            form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";

                        }

                        /** IMPLEMENTED
                            * 
                            **/
                        else if (command == "pengreen")
                        {
                            console.consoleParser("Pen Colour Green - Command found", "valid", form1);
                            form1.CurrentPenColour = "green";

                            form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";

                        }

                        else
                        {
                            console.consoleParser("Pen colour not implemented at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    else if (command == "redgreen")
                    {
                        console.consoleParser("Pen Colour Red/Green Flashing - Command found", "valid", form1);
                        form1.CurrentPenColour = "red/green";

                        form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";
                    }
                    else if (command == "blueyellow")
                    {
                        console.consoleParser("Pen Colour Blue/Yellow Flashing - Command found", "valid", form1);
                        form1.CurrentPenColour = "blue/yellow";

                        form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";
                    }
                    else if (command == "blackwhite")
                    {
                        console.consoleParser("Pen Colour Black/White Flashing - Command found", "valid", form1);
                        form1.CurrentPenColour = "black/white";

                        form1.LabelPenColour.Text = $"Pen Colour: {form1.CurrentPenColour.ToString()}";
                    }


                    /** Implemented
                        *  Global Fill boolean will be used throughout the program.
                        *  If "ToggleFilled is True", each shape will be filled in when "drawn".
                        *  If not, just the outline will be "drawn".
                        **/
                    else if (command == "fill")
                    {
                        console.consoleParser("Fill - Command found", "valid", form1);

                        if (form1.FillToggled == false)
                        {
                            console.consoleParser("Fill is not toggled", "valid", form1);
                            console.consoleParser($"Calling FillToggled toString: {form1.FillToggled.ToString()}", "valid", form1);
                            form1.FillToggled = true;
                            console.consoleParser($"New FillToggled toString: {form1.FillToggled.ToString()}", "valid", form1);

                            form1.LabelFillToggled.Text = $"Fill Toggled: {form1.FillToggled.ToString()}";


                        }
                        else if (form1.FillToggled == true)
                        {
                            console.consoleParser("Fill is toggled", "valid", form1);
                            console.consoleParser($"Calling FillToggled toString: {form1.FillToggled.ToString()}", "valid", form1);
                            form1.FillToggled = false;
                            console.consoleParser($"New FillToggled toString: {form1.FillToggled.ToString()}", "valid", form1);

                            form1.LabelFillToggled.Text = $"Fill Toggled: {form1.FillToggled.ToString()}";
                        }
                        else
                        {
                            console.consoleParser($"Error with: \"{command}\" at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                    }

                    else if (command == "endwhile")
                    {

                        console.consoleParser("Wanting to END LOOP", "System", form1);
                    }

                    else if (command.Contains("while"))
                    {

                        console.consoleParser("Wanting to START LOOP", "System", form1);
                        CommandsAdvanced commandsAdvanced = new CommandsAdvanced();

                        commandsAdvanced.ConditionalParser(commandList, "while", runButton, form1);


                    }



                    else if (command == "endif")
                    {

                        console.consoleParser("Wanting to END IF", "System", form1);
                    }

                    else if (command.Contains("if"))
                    {

                        console.consoleParser("Wanting to START IF", "System", form1);
                        CommandsAdvanced commandsAdvanced = new CommandsAdvanced();

                        commandsAdvanced.ConditionalParser(commandList, "if", runButton, form1);
                    }

                    else if (command.Contains("endmethod"))
                    {
                        console.consoleParser("Wanting to END Method", "System", form1);
                    }

                    else if (command.Contains("method"))
                    {
                        console.consoleParser("Wanting to START METHOD", "System", form1);
                        CommandsAdvanced commandsAdvanced = new CommandsAdvanced();

                        commandsAdvanced.methodParser(commandList, runButton, form1);
                    }

                    else if (command.Contains("("))
                    {

                        console.consoleParser("Searching for Method: '" + command + "'", "System", form1);

                        String[] methodCheck = command.ToString().Replace(" ", "").Replace(")","").Split('(');
                        try
                        {
                            String keyName = methodCheck[0];
                            String keyValue = methodCheck[1];

                            CommandsAdvanced commandsAdvanced = new CommandsAdvanced();

                            console.consoleParser("Method '" + command + "' found", "System", form1);

                            commandsAdvanced.foundMethodParser(keyName, keyValue, true, form1);
                        }
                        catch (KeyNotFoundException e)
                        {
                            console.consoleParser("No Key Found at like: " + form1.CurrentLineNo, "exception", form1);
                        }


                    }

                    /** IMPLEMENTED
                        * 
                        **/
                    else if (command == "run")
                    {
                        console.consoleParser("Run - Command Found", "valid", form1);
                    }

                    /** 
                        * A catch all.
                        **/
                    else
                    {
                        if (command.Contains("+"))
                        {

                            String[] varSumOld = command.Replace(" ", "").Split('=');
                            String[] varSum = varSumOld[1].Split('+');

                            CommandsAdvanced commandsAdvanced = new CommandsAdvanced();

                            String variableName = varSum[0];
                            int variableValue = Int32.Parse(varSum[1]);

                            commandsAdvanced.variableSum(variableName, variableValue, form1);

                        }
                        else if (command.Contains("="))
                        {
                            String[] customVar = command.Replace(" ", "").Split('=');
                            form1.customVariableDict[customVar[0]] = Int32.Parse(customVar[1]);
                            console.consoleParser("Dict entry: \n\tKey: " + customVar[0] + "\tProperty: " + customVar[1] + "added ", "valid", form1);
                        }


                        else
                        {
                            console.consoleParser($"Unknown Command \"{command}\" at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                    }

                    if (commandList.Length != form1.CurrentLineNo)
                    {
                        commandParser(textBoxCode, runButton, true, form1);
                    }
                    else
                    {
                        form1.CurrentLineNo = 0;
                    }

                    if (freshRun != true)
                    {
                        form1.CurrentLineNo = tempCurrentLine;
                    }

                }
                catch (IndexOutOfRangeException e)
                {
                    var command = commandList[0];

                }
            }
            else 
            {
                try
                {
                    var command = commandList[form1.CurrentLineNo];


                    form1.CurrentLineNo++;

                    /** Implemented
                        * Command requires user to input 2 points (X and Y) to move the current "cursor" location to a new one.
                        * This will be contained within this class.
                        **/
                    if (command.Contains("moveto"))
                    {
                        try
                        {
                            console.consoleParser("MoveTo - Command found", "valid", form1);

                            String[] location = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                            try
                            {
                                console.consoleParser($"\nOld CurrentX: {form1.CurrentX} Old CurrentY: {form1.CurrentY}", "valid", form1);
                                int startX = Int32.Parse(location[0]);
                                int startY = Int32.Parse(location[1]);
                                console.consoleParser($"Start X: {startX}, Start Y: {startY}", "valid", form1);

                                console.consoleParser($"\ninputX: {startX} inputY: {startY}\nCurrentX: {form1.CurrentX} CurrentY: {form1.CurrentY}", "valid", form1);

                            }
                            catch (FormatException e)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                            catch (ArgumentOutOfRangeException e)
                            {
                                console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    /** Implemented
                        * Command requires user to input 2 points (X and Y), this will draw a line from the current location to the new location.
                        * The user inputted location will become the "new current" location.
                        * This will call the CanvasDraw Class.
                        **/
                    else if (command.Contains("drawto"))
                    {
                        try
                        {
                            console.consoleParser("DrawTo - Command found", "valid", form1);

                            String[] location = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                            try
                            {
                                int oldX = form1.CurrentX;
                                int oldY = form1.CurrentY;

                                int newX = Int32.Parse(location[0]);
                                int newY = Int32.Parse(location[1]);
                                console.consoleParser($"Old X: {oldX}, Old Y: {oldY}\nNew X: {newX}, New Y: {newY}", "valid", form1);

                                CanvasDraw canvasDraw = new CanvasDraw();
                            }
                            catch (FormatException e)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    /** IMPLEMENTED
                        * When this command is entered, it will clear the canvas.
                        * This will call the Canvas class.
                        **/
                    else if (command == "clear")
                    {

                        console.consoleParser("Clear - Command found", "valid", form1);
                    }

                    /** IMPLEMENTED
                        * 
                        * Reset the cursor location to (0,0)
                        **/
                    else if (command == "reset")
                    {
                        console.consoleParser("Reset - Command found", "valid", form1);
                    }

                    /** IMPLEMENTED
                        * The user will enter a "Height" and "Width" of the rectangle. 
                        * Once detected, everything will be removed apart from the numbers.
                        * Those numbers will be parsed to the Rectangle Class.
                        * The program will "draw" the rectangle starting from the current cursor position.
                        * Will call the Rectangle Class
                        **/
                    else if (command.Contains("rectangle"))
                    {
                        try
                        {
                            console.consoleParser("Rectangle - Command found", "valid", form1);

                            String[] rectangle = command.Remove(0, 9).Replace("(", "").Replace(")", "").Split(',');

                            Rectangle rectangle1 = new Rectangle();

                            try
                            {
                                int width = Int32.Parse(rectangle[0]);
                                int height = Int32.Parse(rectangle[1]);
                                console.consoleParser($"Line 193 Width: {width}, Height: {height}", "valid", form1);
                            }
                            catch (FormatException e)
                            {
                                try
                                {
                                    console.consoleParser("Attempting Custom Dict value", "System", form1);
                                    if (form1.customVariableDict.ContainsKey(rectangle[0]))
                                    {
                                        int width = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                        int height;
                                        console.consoleParser($"Line 211 Width: {width}", "valid", form1);
                                        if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                        {
                                            height = Convert.ToInt32(form1.customVariableDict[rectangle[1]]);


                                            console.consoleParser($"Line 221 Height: {height}", "valid", form1);
                                        }
                                        else
                                        {
                                            height = Int32.Parse(rectangle[1]);
                                        }
                                    }
                                    else if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                    {
                                        int width = Int32.Parse(rectangle[0]);
                                        int height = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                        console.consoleParser($"Line 238 Width: {width}", "valid", form1);

                                    }

                                }
                                catch (FormatException except)
                                {
                                    console.consoleParser($"Line 247. Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);

                                }
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch (FormatException e)
                        {
                            try
                            {
                                console.consoleParser("Rectangle - Command found", "valid", form1);

                                String[] rectangle = command.Remove(0, 9).Replace("(", "").Replace(")", "").Split(',');

                                console.consoleParser("Attempting Custom Dict value", "System", form1);
                                if (form1.customVariableDict.ContainsKey(rectangle[0]))
                                {
                                    int width = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                    int height;
                                    console.consoleParser($"Width: {width}", "valid", form1);
                                    if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                    {
                                        height = Convert.ToInt32(form1.customVariableDict[rectangle[1]]);


                                        console.consoleParser($"Height: {height}", "valid", form1);

                                    }

                                    height = Int32.Parse(rectangle[1]);
                                }
                                else if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                {
                                    int width = Int32.Parse(rectangle[0]);
                                    int height = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                    console.consoleParser($"Width: {width}", "valid", form1);

                                }

                            }
                            catch (FormatException except)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);

                            }
                        }
                        catch
                        {
                            try
                            {
                                console.consoleParser("Rectangle - Command found", "valid", form1);

                                String[] rectangle = command.Remove(0, 9).Replace("(", "").Replace(")", "").Split(',');

                                Rectangle rectangle1 = new Rectangle();
                                if (form1.customVariableDict.ContainsKey(rectangle[1]))
                                {
                                    int width = Int32.Parse(rectangle[0]);
                                    int height = Convert.ToInt32(form1.customVariableDict[rectangle[0]]);
                                    console.consoleParser($"Width: {width}", "valid", form1);

                                }
                            }
                            catch
                            {
                                console.consoleParser($"Line 331 Command Invalid: {command} at like: " + form1.CurrentLineNo, "Exception", form1);

                            }

                        }


                    }

                    /** IMPLEMENTED
                        * A circle will be drawn as an Ellipse.
                        * The circle will require just the radius. 
                        * It will create a rectangle bounding box.
                        * Will call the Circle Class
                        **/
                    else if (command.Contains("circle"))
                    {
                        try
                        {
                            console.consoleParser("Circle - Command found", "valid", form1);

                            String[] circle = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                            Circle circle1 = new Circle();

                            try
                            {
                                int radius = Int32.Parse(circle[0]);
                                console.consoleParser($"Radius: {radius}", "valid", form1);
                            }
                            catch (FormatException e)
                            {
                                try
                                {
                                    console.consoleParser("Attempting Custom Dict value", "System", form1);
                                    if (form1.customVariableDict.ContainsKey(circle[0]))
                                    {
                                        int radius = Convert.ToInt32(form1.customVariableDict[circle[0]]);
                                        console.consoleParser($"Radius: {radius}", "valid", form1);
                                    }

                                }
                                catch (FormatException except)
                                {
                                    console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                                }
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            try
                            {
                                String[] circle = command.Remove(0, 6).Replace("(", "").Replace(")", "").Split(',');

                                Circle circle1 = new Circle();

                                console.consoleParser("Attempting Custom Dict value", "System", form1);
                                if (form1.customVariableDict.ContainsKey(circle[0]))
                                {
                                    int radius = Convert.ToInt32(form1.customVariableDict[circle[0]]);
                                    console.consoleParser($"Radius: {radius}", "valid", form1);
                                }

                            }
                            catch (FormatException except)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    /** IMPLEMENTED
                        * 
                        * 
                        **/
                    else if (command.Contains("triangle"))
                    {
                        try
                        {
                            console.consoleParser("Triangle - Command found", "valid", form1);

                            String[] triangle = command.Remove(0, 8).Replace("(", "").Replace(")", "").Split(',');

                            Triangle triangle1 = new Triangle();

                            try
                            {
                                int lengthAB = Int32.Parse(triangle[0]);
                                int lengthAC = Int32.Parse(triangle[1]);
                                int angleA = Int32.Parse(triangle[2]);

                                console.consoleParser($"LengthAB: {lengthAB}, LengthAC: {lengthAC}, AngleA: {angleA}", "valid", form1);

                            }
                            catch (FormatException e)
                            {
                                console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                            catch
                            {
                                console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                            }
                        }
                        catch (FormatException e)
                        {
                            console.consoleParser($"Execption Thrown: Format Exception. Expected Numbers from: {command} at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            console.consoleParser("Rectangle Out of Range Exception at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                        catch
                        {
                            console.consoleParser($"Command Invalid: {command} at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    else if (command.Contains("pen"))
                    {
                        /** IMPLEMENTED
                            * Each shape class will check what the "Current Pen Colour" is, then check draw dependent on the selected colour.
                            * 
                            * */
                        if (command == "penblack")
                        {
                            console.consoleParser("Pen Colour Black- Command found", "valid", form1);

                        }

                        /** IMPLEMENTED
                            * 
                            **/
                        else if (command == "penblue")
                        {
                            console.consoleParser("Pen Colour Blue - Command found", "valid", form1);

                        }

                        /** IMPLEMENTED
                            * 
                            **/
                        else if (command == "penred")
                        {
                            console.consoleParser("Pen Colour Red - Command found", "valid", form1);

                        }

                        /** IMPLEMENTED
                            * 
                            **/
                        else if (command == "pengreen")
                        {
                            console.consoleParser("Pen Colour Green - Command found", "valid", form1);

                        }

                        else
                        {
                            console.consoleParser("Pen colour not implemented at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    else if (command == "redgreen")
                    {
                        console.consoleParser("Pen Colour Red/Green Flashing - Command found", "valid", form1);
                    }
                    else if (command == "blueyellow")
                    {
                        console.consoleParser("Pen Colour Blue/Yellow Flashing - Command found", "valid", form1);
                    }
                    else if (command == "blackwhite")
                    {
                        console.consoleParser("Pen Colour Black/White Flashing - Command found", "valid", form1);
                    }


                    /** Implemented
                        *  Global Fill boolean will be used throughout the program.
                        *  If "ToggleFilled is True", each shape will be filled in when "drawn".
                        *  If not, just the outline will be "drawn".
                        **/
                    else if (command == "fill")
                    {
                        console.consoleParser("Fill - Command found", "valid", form1);

                        if (form1.FillToggled == false)
                        {
                            console.consoleParser("Fill is not toggled", "valid", form1);
                            console.consoleParser($"Calling FillToggled toString: false", "valid", form1);
                            console.consoleParser($"New FillToggled toString: true", "valid", form1);


                        }
                        else if (form1.FillToggled == true)
                        {
                            console.consoleParser("Fill is toggled", "valid", form1);
                            console.consoleParser($"Calling FillToggled toString: true", "valid", form1);
                            console.consoleParser($"New FillToggled toString: false", "valid", form1);
                        }
                        else
                        {
                            console.consoleParser($"Error with: \"{command}\" at like: " + form1.CurrentLineNo, "Exception", form1);
                        }
                    }

                    else if (command == "endwhile")
                    {

                        console.consoleParser("While Loop Ended", "System", form1);
                    }

                    else if (command.Contains("while"))
                    {

                        console.consoleParser("While Loop Started", "System", form1);


                    }



                    else if (command == "endif")
                    {

                        console.consoleParser("If Statement Ended", "System", form1);
                    }

                    else if (command.Contains("if"))
                    {

                        console.consoleParser("IF statment Started", "System", form1);
                    }



                    /** IMPLEMENTED
                        * 
                        **/
                    else if (command == "run")
                    {
                        console.consoleParser("Run - Command Found", "valid", form1);
                    }

                    /** 
                        * A catch all.
                        **/
                    else
                    {
                        if (command.Contains("+"))
                        {
                            String[] varSumOld = command.Replace(" ", "").Split('=');
                            String[] varSum = varSumOld[1].Split('+');

                            console.consoleParser("Dict Addition: \n\tPart one (old): " + varSumOld[0] + "\tProperty: " + varSumOld[1], "valid", form1);
                            console.consoleParser("Dict Addition: \n\tPart two (new): " + varSum[0] + "\tProperty: " + varSum[1], "valid", form1);
                            CommandsAdvanced commandsAdvanced = new CommandsAdvanced();

                            String variableName = varSum[0];
                            int variableValue = Int32.Parse(varSum[1]);

                        }
                        else if (command.Contains("="))
                        {
                            String[] customVar = command.Replace(" ", "").Split('=');
                            console.consoleParser("Dict entry: \n\tKey: " + customVar[0] + "\tProperty: " + customVar[1] + "added ", "valid", form1);
                        }


                        else
                        {
                            console.consoleParser($"Unknown Command \"{command}\" at like: " + form1.CurrentLineNo, "exception", form1);
                        }
                    }

                    if (commandList.Length != form1.CurrentLineNo)
                    {
                        commandParser(textBoxCode, runButton, true, form1);
                    }
                    else
                    {
                        form1.CurrentLineNo = 0;
                    }

                    if (freshRun != true)
                    {
                        form1.CurrentLineNo = tempCurrentLine;
                    }

                }
                catch (IndexOutOfRangeException e)
                {
                    var command = commandList[0];

                }
            }


        }
    }
}
