﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace C3558551_ASE_Assignment
{
    /// <summary>
    /// Once called, a triangle will be drawn on the canvas. It will be the same colour as current pen colour, and will be filled if fill has been toggled.
    /// </summary>
    class Triangle
    {
        /// <summary>
        /// A rectangle will be drawn with given integers for the LengthAB, LengthAC and AngleA, with the set pen colour and either filled or not filled dependent on the toggle.
        /// </summary>
        /// <param name="LengthAB (int)">A given integer for the Length of side AB</param>
        /// <param name="LengthAC (int)">A given integer for the Length of side AC</param>
        /// <param name="AngleA (int)">A given integer for Angle A</param>
        /// <param name="form1 (Form1)">An object of the main class</param>
        public void drawTriangle(int LengthAB, int LengthAC, int AngleA, Form1 form1)
        {
            Console console = new Console();
            form1.LengthAB = LengthAB;
            form1.LengthAC = LengthAC;
            form1.AngleA = AngleA;

            if (form1.CurrentPenColour == "red/green")
            {
                Thread flashingTriangleRedGreen = new Thread(() => Triangle.redgreen(LengthAB, LengthAC, AngleA, form1.CurrentX, form1.CurrentY, form1));
                flashingTriangleRedGreen.Start();
            }
            else if (form1.CurrentPenColour == "blue/yellow")
            {
                Thread flashingTriangleBlueYellow = new Thread(() => Triangle.blueyellow(LengthAB, LengthAC, AngleA, form1.CurrentX, form1.CurrentY, form1));
                flashingTriangleBlueYellow.Start();
            }
            else if (form1.CurrentPenColour == "black/white")
            {
                Thread flashingTriangleBlackWhile = new Thread(() => Triangle.blackwhite(LengthAB, LengthAC, AngleA, form1.CurrentX, form1.CurrentY, form1));
                flashingTriangleBlackWhile.Start();
            }
            else if (form1.FillToggled == true)
            {
                if (form1.CurrentPenColour == "black") 
                {
                    form1.paintGraphics.FillPolygon(Brushes.Black, new Point[]
                    {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                    });
                    form1.PictureBox1.Refresh();
                }

                else if(form1.CurrentPenColour == "blue")
                {
                    form1.paintGraphics.FillPolygon(Brushes.Blue, new Point[]
                    {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                    });
                    form1.PictureBox1.Refresh();
                }

                else if (form1.CurrentPenColour == "red")
                {
                    form1.paintGraphics.FillPolygon(Brushes.Red, new Point[]
                    {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                    });
                    form1.PictureBox1.Refresh();
                }

                else if (form1.CurrentPenColour == "green")
                {
                    form1.paintGraphics.FillPolygon(Brushes.Green, new Point[]
                    {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                    });
                    form1.PictureBox1.Refresh();
                }

                else
                {
                    console.consoleParser("Fill is toggled True, but can't determain pen colour", "exception", form1);
                }
            }
            

            else if (form1.FillToggled == false)
            {
                if (form1.CurrentPenColour == "black")
                {
                    form1.paintGraphics.DrawPolygon(Pens.Black, new Point[]
                {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
                    form1.PictureBox1.Refresh();
                }

                else if (form1.CurrentPenColour == "blue")
                {
                    form1.paintGraphics.DrawPolygon(Pens.Blue, new Point[]
                {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
                    form1.PictureBox1.Refresh();
                }

                else if (form1.CurrentPenColour == "red")
                {
                    form1.paintGraphics.DrawPolygon(Pens.Red, new Point[]
                {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
                    form1.PictureBox1.Refresh();
                }

                else if (form1.CurrentPenColour == "green")
                {
                    form1.paintGraphics.DrawPolygon(Pens.Green, new Point[]
                {
                    new Point(form1.CurrentX, form1.CurrentY),
                    new Point(form1.CurrentX + LengthAB, form1.CurrentY),
                    new Point((int)(form1.CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(form1.CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
                    form1.PictureBox1.Refresh();
                }

                else
                {
                    console.consoleParser("Fill is toggled False, but can't determain pen colour", "exception", form1);
                }
            }
        }
        public static void redgreen(int LengthAB, int LengthAC, int AngleA, int CurrentX, int CurrentY, Form1 form1)
        {
            form1.paintGraphics.DrawPolygon(Pens.Red, new Point[]
                {
                    new Point(CurrentX, CurrentY),
                    new Point(CurrentX + LengthAB, CurrentY),
                    new Point((int)(CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            form1.paintGraphics.DrawPolygon(Pens.Green, new Point[]
                {
                    new Point(CurrentX, CurrentY),
                    new Point(CurrentX + LengthAB, CurrentY),
                    new Point((int)(CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            redgreen(LengthAB, LengthAC, AngleA, CurrentX, CurrentY, form1);
        }
        public static void blueyellow(int LengthAB, int LengthAC, int AngleA, int CurrentX, int CurrentY, Form1 form1)
        {
            form1.paintGraphics.DrawPolygon(Pens.Blue, new Point[]
                {
                    new Point(CurrentX, CurrentY),
                    new Point(CurrentX + LengthAB, CurrentY),
                    new Point((int)(CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);


            form1.paintGraphics.DrawPolygon(Pens.Yellow, new Point[]
                {
                    new Point(CurrentX, CurrentY),
                    new Point(CurrentX + LengthAB, CurrentY),
                    new Point((int)(CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            blueyellow(LengthAB, LengthAC, AngleA, CurrentX, CurrentY, form1);
        }
        public static void blackwhite(int LengthAB, int LengthAC, int AngleA, int CurrentX, int CurrentY, Form1 form1)
        {

            form1.paintGraphics.DrawPolygon(Pens.Black, new Point[]
                {
                    new Point(CurrentX, CurrentY),
                    new Point(CurrentX + LengthAB, CurrentY),
                    new Point((int)(CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);

            form1.paintGraphics.DrawPolygon(Pens.White, new Point[]
                {
                    new Point(CurrentX, CurrentY),
                    new Point(CurrentX + LengthAB, CurrentY),
                    new Point((int)(CurrentX + LengthAC*Math.Cos(AngleA*Math.PI/180)),
                    (int)(CurrentY + LengthAC*Math.Sin(AngleA*Math.PI/180))),
                });
            if (form1.PictureBox1.InvokeRequired)
            {
                try
                {
                    form1.PictureBox1.Invoke(new MethodInvoker(
                        delegate ()
                        {
                            form1.PictureBox1.Refresh();
                        }));
                }
                catch (ThreadStateException)
                {
                    System.Console.WriteLine("AHHH");
                }

            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            blackwhite(LengthAB, LengthAC, AngleA, CurrentX, CurrentY, form1);
        }

    }
}
