﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C3558551_ASE_Assignment
{
    /**
     * The Canvas Class houses the creating of the Canvas on the main form, as well as holding the location of where the cursor (pointer) is.
     * Each command (in their own class) will talk to this class to "draw".
     * */

    /// <summary>
    /// The Canvas Class houses the creating of the Canvas on the main form, as well as holding the location of where the cursor (pointer) is.<br></br>
    /// Each command(in their own class) will talk to this class to "draw".
    /// </summary>
    public class Canvas : Form1
    {
        /// <summary>
        /// This class will setup the Drawing Canvas.<br></br>
        /// Here, it will get the height and with of the PictureBox and create set a bitmap to the size of those.<br></br>
        /// After the bitmap has been created, it will fill that with a white rectangle (the background).<br></br>
        /// It will then refresh the PictureBox to display the newly created background.
        /// </summary>
        /// <param name="form1 (Form1)">An object of the main class</param>
        public void loadPictureBox(Form1 form1)
        {
            //CanvasDraw canvasDraw = new CanvasDraw();

            Console console = new Console();

            int width = PictureBox1.Width;
            int height = PictureBox1.Height;

            console.consoleParser($"loadedPictureBoxClass START: \nScreen Width: {width}\nScreen Height: {height}", "start", form1);

            form1.paintImage = new Bitmap(width, height);
            console.consoleParser("BitMap Created", "start", form1);

            form1.paintGraphics = Graphics.FromImage(form1.paintImage);
            console.consoleParser("paintGraphics created from Graphics.FromImage(paintImage)", "start", form1);

            form1.paintGraphics.FillRectangle(Brushes.White, CurrentX, CurrentY, width, height);
            console.consoleParser("paintGraphics.FillRectangle", "start", form1);

            form1.PictureBox1.Image = form1.paintImage;
            console.consoleParser("PictureBox1.Image = paintImage", "start", form1);

            //paintGraphics.FillRectangle(Brushes.Black, currentX, currentY, 20, 30);

            form1.PictureBox1.Refresh();
            console.consoleParser("PictureBox1.Refresh()", "start", form1);

            console.consoleParser("loadedPictureBoxClass END\n", "start", form1);

            //canvasDraw.drawLine(140, 240, 480, 320);

            //drawLine(140, 240, 480, 320);


        }
    }
}
