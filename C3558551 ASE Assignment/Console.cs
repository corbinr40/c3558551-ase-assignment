﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace C3558551_ASE_Assignment
{
    class Console
    {
        public void consoleParser(String CommandsOutputs, String CommandsOutputType, Form1 form1)
        {
            if (CommandsOutputType.ToLower().Equals("exception"))
            {
                //System.Console.ForegroundColor = ConsoleColor.Red;
                //System.Console.WriteLine(CommandsOutputs);
                form1.RichTextBoxConsole.SelectionColor = Color.Red;
                form1.RichTextBoxConsole.AppendText($"{CommandsOutputs}\n");
                form1.RichTextBoxConsole.ScrollToCaret();
            }
            else if (CommandsOutputType.ToLower().Equals("valid"))
            {
                //System.Console.ForegroundColor = ConsoleColor.White;
                //System.Console.WriteLine(CommandsOutputs);
                form1.RichTextBoxConsole.SelectionColor = Color.White;
                form1.RichTextBoxConsole.AppendText($"{CommandsOutputs}\n");
                form1.RichTextBoxConsole.ScrollToCaret();
            }
            else if (CommandsOutputType.ToLower().Equals("start"))
            {
                //System.Console.ForegroundColor = ConsoleColor.Cyan;
                //System.Console.WriteLine(CommandsOutputs);
                form1.RichTextBoxConsole.SelectionColor = Color.Cyan;
                form1.RichTextBoxConsole.AppendText($"{CommandsOutputs}\n");
                form1.RichTextBoxConsole.ScrollToCaret();
            }
            else if (CommandsOutputType.ToLower().Equals("system"))
            {
                //System.Console.ForegroundColor = ConsoleColor.Yellow;
                //System.Console.WriteLine(CommandsOutputs);
                form1.RichTextBoxConsole.SelectionColor = Color.Yellow;
                form1.RichTextBoxConsole.AppendText($"{CommandsOutputs}\n");
                form1.RichTextBoxConsole.ScrollToCaret();
            }
            else
            {
                //System.Console.ForegroundColor = ConsoleColor.DarkRed;
                //System.Console.WriteLine($"Incorrect Console Format.\nCommand Output: {CommandsOutputs}\nCommand Type: {CommandsOutputType}");
                form1.RichTextBoxConsole.SelectionColor = Color.DarkRed;
                form1.RichTextBoxConsole.AppendText($"Incorrect Console Format.\nCommand Output: {CommandsOutputs}\nCommand Type: {CommandsOutputType}\n");
                form1.RichTextBoxConsole.ScrollToCaret();
            }
        }
    }
}
