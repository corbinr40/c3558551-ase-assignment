﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

//using C3558551_ASE_Assignment.Properties;

namespace C3558551_ASE_Assignment
{
    /// <summary>
    /// This is where a few of the commands for the user will be called from.
    /// </summary>
    public class CanvasDraw : Form1
    {
        /// <summary>
        /// A user is required to enter an end position (x,y).<br></br>
        /// From that, it will get start X and Y positing and create them as "Points", and then do the same with the user inputted End Points.<br></br>
        /// The program will also determine what the Pen Colour is set to, then draw in the correct Pen Colour. <br></br>
        /// Using the "DrawLine" method from the paintGraphics class, it will draw a line between the two points.<br></br>
        /// Once this has been completed, it will set the Current X and Y positions to what the user previously entered.
        /// </summary>
        /// <param name="startX (int)">An integer of the current, or start, X coordinate.</param>
        /// <param name="startY (int)">An integer of the current, or start, Y coordinate.</param>
        /// <param name="endX (int)">An integer of the user entered X coordinate.</param>
        /// <param name="endY (int)">An integer of the user entered Y coordinate.</param>
        /// <param name="form1 (Form1)">An object of the main class</param>
        public void drawLine(int startX, int startY, int endX, int endY, Form1 form1)
        {

            Point startPoint = new Point(startX, startY);
            Point endPoint = new Point(endX, endY);

            form1.paintGraphics.DrawLine(Pens.Black, startPoint, endPoint);

            form1.CurrentX = endX;
            form1.CurrentY = endY;

            form1.pictureBox1.Refresh();
        }
    }
}
