﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;

namespace C3558551_ASE_Assignment
{
    /// <summary>
    /// Once called, a rectangle will be drawn on the canvas. It will be the same colour as current pen colour, and will be filled if fill has been toggled.
    /// </summary>
    public class Rectangle
    {
        public int StartX;
        public int StartY;

        /// <summary>
        /// A rectangle will be drawn with given integers for the Width and Height, with the set pen colour and either filled or not filled dependent on the toggle.
        /// </summary>
        /// <param name="Width (int)">A given integer for the Width of the rectangle</param>
        /// <param name="Height (int)">A given integer for the Height of the rectangle</param>
        /// <param name="form1 (Form1)">An object of the main class</param>
        public void drawRectangle(int Width, int Height, bool buttonRun, Form1 form1)
        {
            Console console = new Console();
            int StartX = form1.CurrentX;
            int StartY = form1.CurrentY;
            String fillRecText;

            form1.Height = Height;
            form1.Width = Width;
            if (buttonRun == false)
            {
                if (form1.CurrentPenColour == "red/green")
                {
                    Thread flashingRectangleRedGreen = new Thread(() => Rectangle.redgreen(StartX, StartY, Width, Height, form1));
                    flashingRectangleRedGreen.Start();
                }
                else if (form1.CurrentPenColour == "blue/yellow")
                {
                    Thread flashingRectangleBlueYellow = new Thread(() => Rectangle.blueyellow(StartX, StartY, Width, Height, form1));
                    flashingRectangleBlueYellow.Start();
                }
                else if (form1.CurrentPenColour == "black/white")
                {
                    Thread flashingRectangleBlackWhile = new Thread(() => Rectangle.blackwhite(StartX, StartY, Width, Height, form1));
                    flashingRectangleBlackWhile.Start();
                }
                else if (form1.FillToggled == true)
                {
                    if (form1.CurrentPenColour == "black")
                    {
                        //form1.paintGraphics.FillRectangle(Brushes.Black, StartX, StartY, Width, Height);
                        fillRecText = "Brush Type: Black, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "blue")
                    {
                        //form1.paintGraphics.FillRectangle(Brushes.Blue, StartX, StartY, Width, Height);
                        fillRecText = "Brush Type: Blue, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "red")
                    {
                        //form1.paintGraphics.FillRectangle(Brushes.Red, StartX, StartY, Width, Height);
                        fillRecText = "Brush Type: Red, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "green")
                    {
                        //form1.paintGraphics.FillRectangle(Brushes.Green, StartX, StartY, Width, Height);
                        fillRecText = "Brush Type: Green, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }
                    else
                    {
                        console.consoleParser("Fill is toggled True, but can't determain pen colour", "exception", form1);
                    }
                }

                else if (form1.FillToggled == false)
                {
                    if (form1.CurrentPenColour == "black")
                    {
                        //form1.paintGraphics.DrawRectangle(Pens.Black, StartX, StartY, Width, Height);
                        fillRecText = "Fill Type: Black, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "blue")
                    {
                        //form1.paintGraphics.DrawRectangle(Pens.Blue, StartX, StartY, Width, Height);
                        fillRecText = "Fill Type: Blue, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "red")
                    {
                        //form1.paintGraphics.DrawRectangle(Pens.Red, StartX, StartY, Width, Height);
                        fillRecText = "Fill Type: Red, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "green")
                    {
                        //form1.paintGraphics.DrawRectangle(Pens.Green, StartX, StartY, Width, Height);
                        fillRecText = "Fill Type: Green, Start X: " + StartX + ", Start Y: " + StartY + ", Width: " + Width + ", Height: " + Height;
                        console.consoleParser(fillRecText, "valid", form1);

                        form1.PictureBox1.Refresh();
                    }
                    else
                    {
                        console.consoleParser("Fill is toggled False, but can't determain pen colour", "exception", form1);
                    }
                }
            }
            else 
            {
                if (form1.CurrentPenColour == "red/green")
                {
                    Thread flashingRectangleRedGreen = new Thread(() => Rectangle.redgreen(StartX, StartY, Width, Height, form1));
                    flashingRectangleRedGreen.Start();
                }
                else if (form1.CurrentPenColour == "blue/yellow")
                {
                    Thread flashingRectangleBlueYellow = new Thread(() => Rectangle.blueyellow(StartX, StartY, Width, Height, form1));
                    flashingRectangleBlueYellow.Start();
                }
                else if (form1.CurrentPenColour == "black/white")
                {
                    Thread flashingRectangleBlackWhile = new Thread(() => Rectangle.blackwhite(StartX, StartY, Width, Height, form1));
                    flashingRectangleBlackWhile.Start();
                }
                else if (form1.FillToggled == true)
                {
                    if (form1.CurrentPenColour == "black")
                    {
                        form1.paintGraphics.FillRectangle(Brushes.Black, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "blue")
                    {
                        form1.paintGraphics.FillRectangle(Brushes.Blue, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "red")
                    {
                        form1.paintGraphics.FillRectangle(Brushes.Red, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "green")
                    {
                        form1.paintGraphics.FillRectangle(Brushes.Green, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }
                    else
                    {
                        console.consoleParser("Fill is toggled True, but can't determain pen colour", "exception", form1);
                    }
                }

                else if (form1.FillToggled == false)
                {
                    if (form1.CurrentPenColour == "black")
                    {
                        form1.paintGraphics.DrawRectangle(Pens.Black, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "blue")
                    {
                        form1.paintGraphics.DrawRectangle(Pens.Blue, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "red")
                    {
                        form1.paintGraphics.DrawRectangle(Pens.Red, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }

                    else if (form1.CurrentPenColour == "green")
                    {
                        form1.paintGraphics.DrawRectangle(Pens.Green, StartX, StartY, Width, Height);

                        form1.PictureBox1.Refresh();
                    }
                    else
                    {
                        console.consoleParser("Fill is toggled False, but can't determain pen colour", "exception", form1);
                    }
                }
            }
            

        }

        public static void redgreen(int StartX, int StartY, int Width, int Height, Form1 form1)
        {

            form1.paintGraphics.DrawRectangle(Pens.Red, StartX, StartY, Width, Height);

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            form1.paintGraphics.DrawRectangle(Pens.Green, StartX, StartY, Width, Height);

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            redgreen(StartX, StartY, Width, Height, form1);
        }
        public static void blueyellow(int StartX, int StartY, int Width, int Height, Form1 form1)
        {

            form1.paintGraphics.DrawRectangle(Pens.Blue, StartX, StartY, Width, Height);

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            form1.paintGraphics.DrawRectangle(Pens.Yellow, StartX, StartY, Width, Height);

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            blueyellow(StartX, StartY, Width, Height, form1);
        }
        public static void blackwhite(int StartX, int StartY, int Width, int Height, Form1 form1)
        {

            form1.paintGraphics.DrawRectangle(Pens.Black, StartX, StartY, Width, Height);

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            form1.paintGraphics.DrawRectangle(Pens.White, StartX, StartY, Width, Height);

            if (form1.PictureBox1.InvokeRequired)
            {
                form1.PictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        form1.PictureBox1.Refresh();
                    }));
            }
            else
                form1.PictureBox1.Refresh();

            Thread.Sleep(500);
            blackwhite(StartX, StartY, Width, Height, form1);
        }
    }
}
