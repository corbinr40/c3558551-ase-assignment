﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System;
using System.Drawing;
using C3558551_ASE_Assignment;

namespace C3558551_AESAssignment_UnitTest
{
    [TestClass]
    public class Part1UnitTestValid
    {
        Form1 form1 = new Form1();
        Commands commands = new Commands();


        static Bitmap paintImage = new Bitmap(500, 500);
        static Graphics paintGraphics = Graphics.FromImage(paintImage);

        [TestMethod]
        public void TestMethodCommandMoveToValid()
        {
            //arrange
            Commands commands = new Commands();
            //Form1 form1 = new Form1();

            int expectedX = 30;
            int expectedY = 50;

            //act

            commands.commandParser("moveto(30,50)",true ,true, form1);

            //assert

            Assert.AreEqual(expectedX, form1.CurrentX, $"The expected X value is not found. Expected: {expectedX}, Actual: {form1.CurrentX}");
            Assert.AreEqual(expectedY, form1.CurrentY, $"The expected Y value is not found. Expected: {expectedY}, Actual: {form1.CurrentY}");

        }

        [TestMethod]
        public void TestMethodDrawToValid()
        {

            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);


            int expectedX = 60;
            int expectedY = 80;

            //Act
            commands.commandParser("drawto(60,80)", true, true, form1);

            //Assert
            Assert.AreEqual(expectedX, form1.CurrentX, $"The expected X value is not found. Expected: {expectedX}, Actual: {form1.CurrentX}");
            Assert.AreEqual(expectedY, form1.CurrentY, $"The expected X value is not found. Expected: {expectedY}, Actual: {form1.CurrentY}");
        }

        [TestMethod]
        public void TestMethodResetValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            int expectedX = 0;
            int expectedY = 0;

            //Act
            commands.commandParser("moveto(30,70)", true, true, form1);
            commands.commandParser("reset", true, true, form1);

            //Assert
            Assert.AreEqual(expectedX, form1.CurrentX, $"The expected X value is not found. Expected: {expectedX}, Actual: {form1.CurrentX}");
            Assert.AreEqual(expectedY, form1.CurrentY, $"The expected X value is not found. Expected: {expectedY}, Actual: {form1.CurrentY}");
        }

        [TestMethod]
        public void TestMethodRectangleValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            int expectedWidth = 50;
            int expectedHeight = 40;

            //Act
            commands.commandParser("rectangle(50,40)", true, true, form1);

            //Assert
            Assert.AreEqual(expectedWidth, form1.Width, $"The expected X value is not found. Expected: {expectedWidth}, Actual: {form1.Width}");
            Assert.AreEqual(expectedHeight, form1.Height, $"The expected X value is not found. Expected: {expectedHeight}, Actual: {form1.Height}");

        }

        [TestMethod]
        public void TestMethodCircleValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);
            
            int expectedRadius = 30;

            //Act
            commands.commandParser("circle(30)", true, true, form1);

            //Assert
            Assert.AreEqual(expectedRadius, form1.Radius, $"The expected radius value is not found. Expected: {expectedRadius}, Actual: {form1.Radius}");

        }

        [TestMethod]
        public void TestMethodTriangleValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            int expectedLengthAB = 76;
            int expectedLengthAC = 10;
            int expectedAngleA = 50;

            //Act
            commands.commandParser("triangle(76,10,50)", true, true, form1);

            //Assert
            Assert.AreEqual(expectedLengthAB, form1.LengthAB, $"The expected X value is not found. Expected: {expectedLengthAB}, Actual: {form1.LengthAB}");
            Assert.AreEqual(expectedLengthAC, form1.LengthAC, $"The expected X value is not found. Expected: {expectedLengthAC}, Actual: {form1.LengthAC}");
            Assert.AreEqual(expectedAngleA, form1.AngleA, $"The expected X value is not found. Expected: {expectedAngleA}, Actual: {form1.AngleA}");

        }

        [TestMethod]
        public void TestMethodPenBlackColourValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            string expectedPenColour = "black";

            //Act
            commands.commandParser("penblack", true, true, form1);

            //Assert
            Assert.AreEqual(expectedPenColour, form1.CurrentPenColour, $"Pen Colour Expected to be: {expectedPenColour}, but is Actually: {form1.CurrentPenColour}");
        }

        [TestMethod]
        public void TestMethodPenBlueColourValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            string expectedPenColour = "blue";

            //Act
            commands.commandParser("penblue", true, true, form1);

            //Assert
            Assert.AreEqual(expectedPenColour, form1.CurrentPenColour, $"Pen Colour Expected to be: {expectedPenColour}, but is Actually: {form1.CurrentPenColour}");

        }

        [TestMethod]
        public void TestMethodPenRedColourValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            string expectedPenColour = "red";

            //Act
            commands.commandParser("penred", true, true, form1);

            //Assert
            Assert.AreEqual(expectedPenColour, form1.CurrentPenColour, $"Pen Colour Expected to be: {expectedPenColour}, but is Actually: {form1.CurrentPenColour}");

        }

        [TestMethod]
        public void TestMethodPenGreenColourValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            string expectedPenColour = "green";

            //Act
            commands.commandParser("pengreen", true, true, form1);

            //Assert
            Assert.AreEqual(expectedPenColour, form1.CurrentPenColour, $"Pen Colour Expected to be: {expectedPenColour}, but is Actually: {form1.CurrentPenColour}");

        }

        [TestMethod]
        public void TestMethodFillIsTrueValid()
        {
            //Arrange
            paintGraphics.FillRectangle(Brushes.White, 0, 0, 500, 500);

            //Act
            commands.commandParser("fill", true, true, form1);

            //Assert
            Assert.IsTrue(form1.FillToggled, $"The expected output is True, but was: {form1.FillToggled}");
        }
    }

    [TestClass]
    public class Part2UnitTest
    {
        [TestMethod]
        public void TestMethodLoops()
        {
            //Arrange

            //Act

            //Assert
            Assert.Fail();
        }

        [TestMethod]
        public void TestMethodIfStatement()
        {
            //Arrange

            //Act

            //Assert
            Assert.Fail();
        }
    }
}
